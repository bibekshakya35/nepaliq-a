var placestory = {
    "place": undefined,
    "story": undefined,
    "picture": undefined,
    "userid": undefined,
    "name": undefined,
    "createdDate": new Date()
}

module.exports.checkIfObjectIsWellForPlace = function (obj2) {
    for (let i in placestory) {
        if (!obj2.hasOwnProperty(i)) {
            return false;
        }
    } 
    return true;

}