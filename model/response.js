module.exports.error = function(message){
 return new Response(401,message,"");
}
module.exports.get = function(data){
  return new Response(200,"",JSON.stringify(data));
}
module.exports.success = function(message,data){
    return new Response(200,message,JSON.stringify(data));
}
module.exports.updated = function(message,data){
    return new Response(200,message,"");
}
module.exports.created = function(message,data){
    return new Response(201,message,JSON.stringify(data));
}
function Response(code,message,data){
    this.code = code;
    this.message  = message;
    this.data = data;
}