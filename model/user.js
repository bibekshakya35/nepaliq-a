var mongoose = require('mongoose');

var bcrypt = require('bcrypt-nodejs');


var userSchema = mongoose.Schema({
        password: String,
        name: String,
        email: String,
        avatarUrl: String,
        reputation: Number,
        mobile: String,
        bloodgroup: String,
        location: String,
        city: String,
        token: String,
        userType: String,
        userid:String,
        username:String,
        createdOn:Date
});


// checking if password is valid
userSchema.methods.validPassword = function(password) {
    console.log("OK");
    return bcrypt.compareSync(password, this.password);
};

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);