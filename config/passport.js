//This is where we configure our Strategy for local, facebook, twitter, and google. 
//This is also the file where we will create the serializeUser and deserializeUser functions to store our user in session.
//sign up
//load all the things we need

var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var TwitterStrategy = require('passport-twitter').Strategy;
var bcrypt = require('bcrypt-nodejs');
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var Response = require('../model/response');
//load up the user models
var User = require('../model/user');

//load the auth variable
var configAuth = require('./auth');
//expose this function to our app using module.export
module.exports = function (passport) {
    //passport session setup
    //required for persistent login sessions
    //passport needs ability to serialize and unserialize users out of session

    // used to serialize the user for the session
    passport.serializeUser(function (user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function (id, done) {
        User.findById(id, function (err, user) {
            done(err, user);
        });
    });

    //local sign up 
    // we are using named strategies since we have one for login and one for sign up
    //by default , if there were no name it would just be called login
    passport.use('local-signup', new LocalStrategy({
        //by default local strategy uses username and password and we will overide with emaild id;
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true //allows us to pass back the entire request to callback
    },
        (req, email, password, done) => {

            //asynchronous 
            //User.findOne wont fire unless data is sent callback

            process.nextTick(() => {
                //find a user whose email is the same as the forms email
                //we are checking to see if the user typing login already exists
                User.findOne({ 'email': email }, (err, user) => {
                    if (err) {
                        return done(null, false, req.flash('failure', "Failure to sign up"));
                    }
                    //check to see if there already iuser with that emaild
                    if (user) {

                        return done(null, false, req.flash('failure', "Already Taken"));
                    }
                    else {
                        let user = req.body;
                        var newUser = new User();
                        newUser.email = user.email;
                        newUser.name = user.name;
                        newUser.userid = user.email;
                        newUser.userType = 'local';
                        newUser.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(8), null);
                        newUser.reputation = 5;
                        newUser.createdOn = new Date();
                        //save the user
                        newUser.save((err) => {
                            if (err) {
                                return done(null, false, req.flash('failure', "Failure to sign up"));
                            }
                            console.log("user joined prasnottar ", newUser.name);
                            req.flash("success", newUser);
                            return done(null, newUser);
                        });
                    }
                })
            })
        }));





    passport.use(new FacebookStrategy({

        // pull in our app id and secret from our auth.js file
        clientID: configAuth.facebookAuth.clientID,
        clientSecret: configAuth.facebookAuth.clientSecret,
        callbackURL: configAuth.facebookAuth.callbackURL,
        profileFields: ['id', 'email', 'gender', 'link', 'locale', 'name', 'timezone', 'updated_time', 'verified']

    },

        // facebook will send back the token and profile
        function (token, refreshToken, profile, done) {
            // asynchronous
            process.nextTick(function () {
                // find the user in the database based on their facebook id
                User.findOne({ 'userid': profile.id }, function (err, user) {

                    // if there is an error, stop everything and return that
                    // ie an error connecting to the database
                    if (err)
                        return done(err);

                    // if the user is found, then log them in
                    if (user) {
                        return done(null, user); // user found, return that user
                    } else {

                        // if there is no user found with that facebook id, create them
                        var newUser = new User();
                        newUser.userType = 'facebook';
                        // set all of the facebook information in our user model
                        newUser.userid = profile.id; // set the users facebook id                   
                        newUser.token = token; // we will save the token that facebook provides to the user                    
                        newUser.name = profile.name.givenName + ' ' + profile.name.familyName; // look at the passport user profile to see how names are returned
                        newUser.createdOn = new Date();
                        if (profile.emails !== undefined) {
                            newUser.email = profile.emails[0].value;
                        }
                        // facebook can return multiple emails so we'll take the first
                        newUser.reputation = 5;

                        // save our user to the database
                        newUser.save(function (err) {
                            // if successful, return the new user
                            console.log("user joined prasnottar ", newUser.name);
                            return done(null, newUser);
                        });
                    }

                });
            });

        }));
    passport.use(new TwitterStrategy({

        consumerKey: configAuth.twitterAuth.consumerKey,
        consumerSecret: configAuth.twitterAuth.consumerSecret,
        callbackURL: configAuth.twitterAuth.callbackURL

    },
        function (token, tokenSecret, profile, done) {

            // make the code asynchronous
            // User.findOne won't fire until we have all our data back from Twitter
            process.nextTick(function () {

                User.findOne({ 'userid': profile.id }, function (err, user) {

                    // if there is an error, stop everything and return that
                    // ie an error connecting to the database
                    if (err)
                        return done(err);

                    // if the user is found then log them in
                    if (user) {
                        return done(null, user); // user found, return that user
                    } else {
                        // if there is no user, create them
                        var newUser = new User();

                        // set all of the user data that we need
                        newUser.userid = profile.id;
                        newUser.token = token;
                        newUser.username = profile.username;
                        newUser.name = profile.displayName;
                        newUser.reputation = 5;
                        newUser.userType = 'twitter';
                        newUser.createdOn = new Date();
                        newUser.avatarUrl = profile.photos[0].value.replace('_normal', '');
                        // save our user into the database
                        newUser.save(function (err) {
                            if (err)
                                throw err;
                            console.log("user joined prasnottar ", newUser.name);
                            return done(null, newUser);
                        });
                    }
                });

            });

        }));

    passport.use(new GoogleStrategy({

        clientID: configAuth.googleAuth.clientID,
        clientSecret: configAuth.googleAuth.clientSecret,
        callbackURL: configAuth.googleAuth.callbackURL,

    },
        function (token, refreshToken, profile, done) {

            // make the code asynchronous
            // User.findOne won't fire until we have all our data back from Google
            process.nextTick(function () {

                // try to find the user based on their google id
                User.findOne({ 'userid': profile.id }, function (err, user) {
                    if (err)
                        return done(err);

                    if (user) {

                        // if a user is found, log them in
                        return done(null, user);
                    } else {
                        // if the user isnt in our database, create a new user
                        var newUser = new User();

                        // set all of the relevant information
                        newUser.userid = profile.id;
                        newUser.token = token;
                        newUser.name = profile.displayName;
                        newUser.email = profile.emails[0].value; // pull the first email
                        newUser.reputation = 5;
                        newUser.userType = 'google';
                        newUser.createdOn = new Date();
                        newUser.avatarUrl = profile._json['picture'];
                        // save the user
                        newUser.save(function (err) {
                            if (err)
                                throw err;
                            console.log("user joined prasnottar ", newUser.name);
                            return done(null, newUser);
                        });
                    }
                });
            });

        }));


};

