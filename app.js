var express = require('express');

var path = require('path');

var cookieParser = require('cookie-parser');

var logger = require('morgan');

var bodyParser = require('body-parser');

var exphbs = require('express-handlebars');

var expressValidator = require('express-validator');

var flash = require('connect-flash');

var session = require('express-session');

var mongoose = require('mongoose');
var passport = require('passport');

var localStrategy = require('passport-local'), Strategy;

var users = require('./routes/users');
var posts = require('./routes/posts');
var bloods = require('./routes/bloods');
var index = require('./routes/index');
var loggers = require('./routes/loggers');
var categories = require('./routes/categories');
var nearby = require("./routes/nearby");
var configDB = require('./model/config.js');
var chat = require('./routes/chat');

mongoose.connect(configDB.dbUrl);

//init application
var app = express();

//view Engine
//view engine 
app.set('views', __dirname + '/views');

app.set('view engine', 'ejs');
//render file with html extention
app.engine('html', require('ejs').renderFile);

app.use(express.static(__dirname + '/views/pages/'));

//handler image upload
// var mongo = require('mongodb');
// var db = require('monk')('localhost/nepaliq-a');



//set static folder

// var mongoose = require('mongoose');

// var configDB = require('./model/config');


//body parser
//cookie parser middleware
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(cookieParser());


//express session
app.use(session({
    secret: 'iamwhatiam',
    saveUninitialized: true,
    resave: true
}));

app.use(passport.initialize());
app.use(passport.session());
app.use(flash());
//morgan dev setup 
// app.use(morgan('dev'));

//express validator
//https://github.com/ctavan/express-validator
app.use(expressValidator({
    errorFormatter: function (param, msg, value) {
        var namespace = param.split('.')
            , root = namespace.shift(),
            formParam = root;
        while (namespace.length) {
            formParam += '[' + namespace.shift() + ']';
        }
        return {
            param: formParam,
            msg: msg,
            value: value
        }
    }
}));


//connect   flash 

app.use(flash());

//global variable
app.use(function (req, res, next) {
    res.locals.success_msg = req.flash('success_msg');
    res.locals.error_msg = req.flash('error_msg');
    res.locals.error = req.flash('error');
    next();
});


//make out database acceble in any routes
// app.use(function(req,res,next){
//     req.db = db;
//     next();
// });

// app.use('/',index);
// app.use('/api',users);


//socket connection
var http = require('http').createServer(app);
var io = require("socket.io")(http);
var mongojs = require('mongojs');
var dbUrl = "mongodb://bibek:123@ds133241.mlab.com:33241/chat";
var db = mongojs(dbUrl, ['group_chat']);
io.on("connection", (socket) => {
    console.log("user is connected");
    socket.on("adduser", function (data) {
        var username = data.userName;
        var room = data.room;
        socket.userid = data.userid;
        socket.userName = data.userName;
        socket.room = data.room
        socket.room_name = data.room_name;
        socket.join(data.room);
        io.emit("updatechat", { "joined": username + " has been joined room " });
    });
    socket.on("add-message", (data) => {
        db.group_chat.insert(data);
        console.log("...d.ata", data);
        io.emit("message", data);
    });
    socket.on("disconnect", () => {
        io.emit("updatechat", { "left": socket.userName + " has left the room" });
    });

});
//end


app.use('/', index);
app.use('/api', posts);
app.use('/api', bloods);
app.use('/api', users);
app.use('/api', loggers);
app.use("/api", categories);
app.use("/api", nearby);
app.use("/api", chat);
require('./routes/login.js')(app, passport);
//configuration for passport then pass it in to our passport.js file for it to be configured.
//then we pass it to app/routes.js file for it to used in routes
require('./config/passport.js')(passport);

//set the port
app.set('port', (process.env.PORT || 80));

http.listen(app.get('port'), () => {
    console.log('Server start on the port ' + app.get('port'));

});









