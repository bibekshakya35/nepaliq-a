import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { CustomHeader } from "../_utils/customheader";
@Injectable()
export class GenericService {
    constructor(private _http: Http) { }
    getAllResources(url: string) {
        return this._http
            .get(url, CustomHeader.produceCustomHeader())
            .map((reponse: Response) => reponse.json());
    }
    getResourceByUniqueCode(url: string, code: any) {
        return this._http
            .get(url + "/" + code, CustomHeader.produceCustomHeader())
            .map((response: Response) => response.json());
    }
    addNewResource(url: string, object: any) {
        console.log("dasdsadsa" + object);
        return this._http
            .post(url, object, CustomHeader.produceCustomHeader())
            .map((response: Response) => response.json());
    }
    editResource(url: string, code: any, object: any) {
        return this._http
            .put(url + "/" + code, object, CustomHeader.produceCustomHeader())
            .map((response: Response) => response.json());
    }
    editRes(url: string, object: any) {
        return this._http
            .put(url, object, CustomHeader.produceCustomHeader())
            .map((response: Response) => response.json());
    }
    resetPassword(url: string) {
        return this._http
            .put(url, "", CustomHeader.produceCustomHeader())
            .map((response: Response) => response.json());
    }
    patchResource(url: string, object: any) {
        return this._http
            .patch(url, object, CustomHeader.produceCustomHeader())
            .map((response: Response) => response.json());
    }
    getTen(url: string) {
        return this._http
            .get(url, CustomHeader.produceCustomHeader()).
            
            .map((reponse: Response) => reponse.json());
    }

}