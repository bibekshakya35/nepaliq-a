import { Component, OnInit } from '@angular/core';
import { FormBuilder, AbstractControl, FormGroup, Validators, FormControl, FormArray, ReactiveFormsModule } from '@angular/forms';
import { Blood } from "../../models/blood";

import { BreadcrumbService } from '../../services/breadcrumb.service';
import { AlertService, GenericService } from "../../services/index";
import { UrlConfig } from "../../_config/url.cofig";
import { Response } from "../../models/response";
import { Router, ActivatedRoute } from "@angular/router";
import { PreCondition } from '../../_utils/precheckcondition';

@Component({
  selector: 'app-blood-donar',
  templateUrl: './blood-donar.component.html',
  styleUrls: ['./blood-donar.component.css']
})
export class BloodDonarComponent implements OnInit {
  bloods:Blood[];
  bloodGroup = ["O−", "O+", "A−", "A+", "B−", "B+", "AB−", "AB+"];
  locations: Location[];
  state: string[];
  response: Response;
  public data;
  public filterQuery = "";
  public rowsOnPage = 10;
  public sortBy = "email";
  public sortOrder = "asc";
  constructor(
    private bloodServ: GenericService,
    private _alertService: AlertService,
  ) {


  }
  loadLocation() {
    // this._questionSevice.getJsonFile("api/location/locations.json")
    //   .subscribe(
    //   res => {
    //     this.locations = <Location[]>res;
    //   }
    //   );

  }
  ngOnInit() {
    this.loadBlood();
  }
  loadBlood() {
    this.bloodServ.getTen(UrlConfig.BASE_URL + UrlConfig.BLOOD)
      .subscribe(
      res => {
        this.response = <Response>res;
        this.bloods =this.response.data.slice(0,9);
        
        console.log("bloodddddddddddddddd10"+this.bloods);
      }
      );
  }
  public toInt(num: string) {
    return +num;
  }

  public sortByWordLength = (a: any) => {
    return a.city.length;
  }
  hack(val) {
  return Array.from(val);
}
}
