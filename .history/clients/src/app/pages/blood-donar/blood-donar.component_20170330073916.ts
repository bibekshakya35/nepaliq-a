
import { FormBuilder, AbstractControl, FormGroup, Validators, FormControl, FormArray, ReactiveFormsModule } from '@angular/forms';
import { Blood } from "../../models/blood";

import { BreadcrumbService } from '../../services/breadcrumb.service';
import { AlertService, GenericService } from "../../services/index";
import { UrlConfig } from "../../_config/url.cofig";
import { Response } from "../../models/response";
import { Router, ActivatedRoute } from "@angular/router";
import { PreCondition } from '../../_utils/precheckcondition';
import { Component, ViewContainerRef, OnInit } from '@angular/core';
import { Overlay } from 'angular2-modal';
import { Modal } from 'angular2-modal/plugins/bootstrap';
@Component({
  selector: 'app-blood-donar',
  templateUrl: './blood-donar.component.html',
  styleUrls: ['./blood-donar.component.css']
})
export class BloodDonarComponent implements OnInit {
  bloods: Blood[];
  blood: Blood;
  available: string;
  bloodGroup = ["O−", "O+", "A−", "A+", "B−", "B+", "AB−", "AB+"];
  locations: Location[];
  state: string[];
  response: Response;
  isBloodSearchChange: boolean = false;
  public filterQuery = "";
  nameOfthePerson
  public rowsOnPage = 10;
  public sortBy = "name";
  public sortOrder = "asc";
  constructor(
    private bloodServ: GenericService,
    private _alertService: AlertService,
    overlay: Overlay,
    vcRef: ViewContainerRef,
    public modal: Modal) {
    overlay.defaultViewContainer = vcRef;
  }
  onBloodSearchChanged() {
    this.isBloodSearchChange = true;
  }
  loadLocation() {
    // this._questionSevice.getJsonFile("api/location/locations.json")
    //   .subscribe(
    //   res => {
    //     this.locations = <Location[]>res;
    //   }
    //   );

  }
  ngOnInit() {
    this.loadBlood();
  }
  loadBlood() {
    this.bloodServ.getTen(UrlConfig.BASE_URL + UrlConfig.BLOOD)
      .subscribe(
      res => {
        setTimeout(() => {
          this.response = <Response>res;

          this.bloods = JSON.parse(this.response.data);
          console.log("bloodddddddddddddddd10" + this.bloods);
        }, 1000);

      }
      );
  }
  public toInt(num: string) {
    return +num;
  }

  public sortByWordLength = (a: any) => {
    return a.city.length;
  }
  onClickOfDonar(blood) {
    this.blood = blood;
    this.available = this.blood.status === true ? "Available" : "Unavailable";
    console.log("data" + this.blood.location);
    this.modal.alert()
      .size('sm')
      .isBlocking(true)
      .showClose(true)
      .keyboard(27)
      .dialogClass('modal-dialog')
      .headerClass('modal-header')
      .titleHtml('<h4 class="text-info text-center">Blood Donar Information</h4>')
      .body(`
        <div class="e2e-trusted-url">
          <div class="row">
            <div class="col-md-6">Name : </div>
            <div class="col-md-6">`+ this.checkIfAvailable(this.blood.name) + `</div>
            <br>
            <hr style="border:5px black solid;">
          </div>
          <div class="row">
            <div class="col-md-6">Blood Group : </div>
            <div class="col-md-6">`+ this.checkIfAvailable(this.blood.bloodgroup) + `</div>
            <br>
           <hr style="border:5px black solid;">
          </div>
           <div class="row">
               <div class="col-md-6">Mobile Number : </div>
               <div class="col-md-6">`+ this.checkIfAvailable(this.blood.emailid) + `</div>
               <br>
               <hr style="border:5px black solid;">
          </div>
          <div class="row">
               <div class="col-md-6">Address : </div>
               <div class="col-md-6">`+ this.checkIfAvailable(this.blood.location) + `</div>
               <br>
               <hr style="border:5px black solid;">
          </div>
          <div class="row">
               <div class="col-md-6">City : </div>
               <div class="col-md-6">`+ this.checkIfAvailable(this.blood.city) + `</div>
               <br>
               <hr style="border:5px black solid;">
          </div>
          <div class="row">
               <div class="col-md-6">Mobile Number : </div>
               <div class="col-md-6">`+ this.checkIfAvailable(this.blood.mobile) + `</div>
               <br>
               <hr style="border:5px black solid;">
          </div>
          <div class="row">
               <div class="col-md-6">Landline Number : </div>
               <div class="col-md-6">`+ this.checkIfAvailable(this.blood.landline) + `</div>
               <br>
               <hr style="border:5px black solid;">
          </div>
          <div class="row">
               <div class="col-md-6">Mobile Number : </div>
               <div class="col-md-6">`+ this.available + `</div>
               <br>
               <hr style="border:5px black solid;">
          </div>
         
        </div>
    `)
      .footerClass('modal-footer')
      .okBtn('OK')
      .okBtnClass('btn btn-info')
      .open();
  }
  checkIfAvailable(data: any): any {
    if (typeof data === "string") {
      if (data === "") {
        return "Not Provided";
      }
    }
    return data;

  }
}
