import { Component, OnInit } from '@angular/core';
import { FormBuilder, AbstractControl, FormGroup, Validators, FormControl, FormArray, ReactiveFormsModule } from '@angular/forms';
import { Blood } from "../../models/blood";

import { BreadcrumbService } from '../../services/breadcrumb.service';
import { AlertService, GenericService } from "../../services/index";
import { UrlConfig } from "../../_config/url.cofig";
import { Response } from "../../models/response";
import { Router, ActivatedRoute } from "@angular/router";
import { PreCondition } from '../../_utils/precheckcondition';

@Component({
  selector: 'app-blood-donar',
  templateUrl: './blood-donar.component.html',
  styleUrls: ['./blood-donar.component.css']
})
export class BloodDonarComponent implements OnInit {
  bloods:Blood[]=<Blood[]>[
  {
    "_id": "58db0e05661a6d1c078e9ce5",
    "rowid": 1,
    "name": "Bibek Shakya",
    "location": "hidetol",
    "mobile": 9843598726,
    "bloodgroup": "B+",
    "emailid": "bibekshakya35@gmail.com",
    "landline": "",
    "city": "kathmandu",
    "status": true
  },
  {
    "_id": "58db0e05661a6d1c078e9ce6",
    "rowid": 2,
    "name": "Rupak Nepali",
    "location": "harisiddhi",
    "mobile": 9849188514,
    "bloodgroup": "A+",
    "emailid": "nepal.rupak@gmail.com",
    "landline": "",
    "city": "lalitpur",
    "status": true
  }];
  bloodGroup = ["O−", "O+", "A−", "A+", "B−", "B+", "AB−", "AB+"];
  locations: Location[];
  state: string[];
  response: Response;
  public data;
  public filterQuery = "";
  public rowsOnPage = 10;
  public sortBy = "email";
  public sortOrder = "asc";
  constructor(
    private bloodServ: GenericService,
    private _alertService: AlertService,
  ) {


  }
  loadLocation() {
    // this._questionSevice.getJsonFile("api/location/locations.json")
    //   .subscribe(
    //   res => {
    //     this.locations = <Location[]>res;
    //   }
    //   );

  }
  ngOnInit() {
    this.loadBlood();
  }
  loadBlood() {
    // this.bloodServ.getAllResources(UrlConfig.BASE_URL + UrlConfig.BLOOD)
    //   .subscribe(
    //   res => {
    //     this.response = <Response>res;
    //     console.log(this.response.data);
    //     this.bloods =this.response.data;
    //     console.log("bloodddddddddddddddd"+this.bloods);
    //   }
    //   )
  }
  public toInt(num: string) {
    return +num;
  }

  public sortByWordLength = (a: any) => {
    return a.city.length;
  }
  hack(val) {
  return Array.from(val);
}
}
