import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
    name: "dataFilter"
})
export class DataFilterPipe implements PipeTransform {
transform(array: Blood[], filterBy: string): VehicleEntry[] {
        filterBy = filterBy ? filterBy.toLocaleLowerCase() : null;
        return filterBy ? vehicles.filter((vehicle: VehicleEntry) =>
            vehicle.vehicleNumber.toLocaleLowerCase().indexOf(filterBy) !== -1
        ) : vehicles;
    }
}