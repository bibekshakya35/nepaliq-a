import { Pipe, PipeTransform } from "@angular/core";
import { Blood } from "../../models/blood";
@Pipe({
    name: "dataFilter"
})
export class DataFilterPipe implements PipeTransform {
    transform(array: Blood[], filterBy: string[]): Blood[] {
        let bloodGroupData =filterBy[0];
        bloodGroupData = bloodGroupData ? bloodGroupData.toLocaleLowerCase() : null;
        return bloodGroupData ? array.filter((blood: Blood) =>
            blood.bloodgroup.toLocaleLowerCase().indexOf(bloodGroupData) !== -1
        ) : array;
    }
}