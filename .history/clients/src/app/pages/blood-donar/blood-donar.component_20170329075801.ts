import { Component, OnInit } from '@angular/core';
import { FormBuilder, AbstractControl, FormGroup, Validators, FormControl, FormArray, ReactiveFormsModule } from '@angular/forms';
import { Blood } from "../../models/blood";

import { BreadcrumbService } from '../../services/breadcrumb.service';
import { AlertService, GenericService } from "../../services/index";
import { UrlConfig } from "../../_config/url.cofig";
import { Response } from "../../models/response";
import { Router, ActivatedRoute } from "@angular/router";
import { PreCondition } from '../../_utils/precheckcondition';

@Component({
  selector: 'app-blood-donar',
  templateUrl: './blood-donar.component.html',
  styleUrls: ['./blood-donar.component.css']
})
export class BloodDonarComponent implements OnInit {
  bloodDonarForm: FormGroup;
  bloodGroup = ["O−","O+","A−","A+","B−","B+","AB−","AB+"];
  locations : Location[];
  state : string[];
  constructor(
    private _questionSevice: GenericService,
    private _alertService: AlertService,
  ) { 

      
  }
  loadLocation(){
    // this._questionSevice.getJsonFile("api/location/locations.json")
    //   .subscribe(
    //   res => {
    //     this.locations = <Location[]>res;
    //   }
    //   );

  }
  ngOnInit() {
    
   
  }
  
}
