import { Pipe, PipeTransform } from "@angular/core";
import { Blood } from "../../models/blood";
@Pipe({
    name: "dataFilter"
})
export class DataFilterPipe implements PipeTransform {
    transform(array: Blood[], filterBy: string[]): Blood[] {
        let bloodGroupData =filterBy[0];
        let name =filterBy[1];
        bloodGroupData = bloodGroupData ? bloodGroupData.toLocaleLowerCase() : null;
        return  array.filter((blood: Blood) =>
            blood.bloodgroup.toLocaleLowerCase().indexOf(bloodGroupData) !== -1||
            blood.name.toLocaleLowerCase().indexOf(name)!==-1
        );
    }
}