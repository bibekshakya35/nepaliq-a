import { Component, OnInit } from '@angular/core';
import { FormBuilder, AbstractControl, FormGroup, Validators, FormControl, FormArray, ReactiveFormsModule } from '@angular/forms';
import { Blood } from "../../models/blood";

import { BreadcrumbService } from '../../services/breadcrumb.service';
import { AlertService, GenericService } from "../../services/index";
import { UrlConfig } from "../../_config/url.cofig";
import { Response } from "../../models/response";
import { Router, ActivatedRoute } from "@angular/router";
import { PreCondition } from '../../_utils/precheckcondition';

@Component({
  selector: 'app-blood-donar',
  templateUrl: './blood-donar.component.html',
  styleUrls: ['./blood-donar.component.css']
})
export class BloodDonarComponent implements OnInit {
  bloodDonarForm: FormGroup;
  bloodGroup = ["O−","O+","A−","A+","B−","B+","AB−","AB+"];
  locations : Location[];
  state : string[];
  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _questionSevice: GenericService,
    private _alertService: AlertService,
    private _fb: FormBuilder,
    private breadServ: BreadcrumbService
  ) { 

      
  }
  loadLocation(){

  }
  ngOnInit() {
    this.loadLocation();
    this.buidForm();
  }
  public buidForm() {
    this.bloodDonarForm = this._fb.group(
      {
        bloodgroup: ['', [Validators.required]],
        location: [''],
        city: ['']
      }
    );
    this.bloodDonarForm.valueChanges
      .subscribe(data => this.onValueChanged(data));
    this.onValueChanged();
  }
  onValueChanged(data?: any) {
    this._alertService.clear();
    if (!this.bloodDonarForm) { return; }
    const form = this.bloodDonarForm;

    for (const field in this.questionAddErrors) {
      // clear previous error message (if any)
      this.questionAddErrors[field] = '';
      const control = form.get(field);

      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.questionAddErrors[field] += messages[key] + ' ';
        }
      }
    }
  }
  questionAddErrors = {
    bloodgroup: ""
  }
  validationMessages = {
    'bloodgroup': {
      'required': 'Blood Group is required.'
    }
  };
  onSelect(city) {
    for(let locationOBj of this.locations){
      if(locationOBj.city==city){

      }
    }
  }
  searchDonar(){
    console.log(JSON.stringify(this.bloodDonarForm.value));
  }
}
