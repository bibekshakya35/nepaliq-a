import { Pipe, PipeTransform } from "@angular/core";
import { Blood } from "../../models/blood";
@Pipe({
    name: "nameFilter"
})
export class DataFilterPipe implements PipeTransform {
    transform(array: Blood[], filterBy: string): Blood[] {
        filterBy = filterBy ? filterBy.toLocaleLowerCase() : null;
        return filterBy ? array.filter((blood: Blood) =>
            blood.blonameodgroup.toLocaleLowerCase().indexOf(filterBy) !== -1
        ) : array;
    }
}