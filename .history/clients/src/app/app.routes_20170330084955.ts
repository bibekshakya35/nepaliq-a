import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanActivateGuard } from './services/guard.service';
import {AddQuestionComponent} from './pages/add-question/add-question.component';
import { HomeComponent } from './pages/home/home.component';
import { LayoutsAuthComponent } from './pages/layouts/auth/auth';
import { ShowQuestionComponent } from './pages/show-question/show-question.component';

import { BloodDonarComponent } from './pages/blood-donar/blood-donar.component';
const routes: Routes = [
  // logged routes
  {
    canActivate: [CanActivateGuard],
    children: [
      {
        canActivate: [CanActivateGuard],
        component: HomeComponent,
        path: 'prasna'
      },
      {
        canActivate: [CanActivateGuard],
        component: AddQuestionComponent,
        path: 'sodumprasna'
      },
      {
        canActivate: [CanActivateGuard],
        component: ShowQuestionComponent,
        path: 'prasna/uttar/:id'
      },
      {
        canActivate: [CanActivateGuard],
        component: BloodDonarComponent,
        path: 'raktadan'
      }
    ],
    component: LayoutsAuthComponent,
    path: '',
  },
  // not logged routes
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
