export class Blood{
    rowid : number;
    name : string;
    location : string;
    mobile : string;
    bloodgroup : string;
    emailid: string;
    status: boolean;
    landline : string;
    city:string;
    _id:string;
}