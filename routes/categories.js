var express = require('express');
var router = express.Router();

var cors = require('cors');
router.use(cors());
router.options('*', cors());

var mongoose = require('mongoose');
var Response = require('../model/response');
//bring mongo js
var mongojs = require('mongojs');
//connect with database
var config = require('../model/config');

var db = mongojs(config.dbUrl, ['categories']);

router.get("/categories/nocount", (req, res, next) => {
    db.categories.find({}, function (err, categories) {
        if (err) {
            res.send(err);
        }
        let categoriesList = [];
        categories.forEach(function (element) {
            categoriesList.push(element.name);
        }, this);
        res.json(Response.get(categoriesList));
    });
})
router.get('/categories/most/:limit', (req, res) => {
    var limit = parseInt(req.params.limit);
    db.categories.find().sort({ "count": -1 }).limit(limit, function (err, category) {
        res.json(Response.get(category));
    });
});
router.put('/categories', (req, res, next) => {
    var categoriesList = req.body;
    var sortedAndLowerCase = [];
    categoriesList.forEach(function (element) {
        sortedAndLowerCase.push(element.replace(/\s/g, '').toLocaleLowerCase());
    });
    sortedAndLowerCase.forEach((e) => {
        db.categories.findAndModify({
            query: { name: e },
            update: { $inc: { count: 1 } },
            upsert: true
        }, (err, category) => {
        }
        );
    });
    return res.json(Response.success({}, {}));
});

module.exports = router;