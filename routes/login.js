var Response = require('../model/response');
var User = require('../model/user');
var config = require('../model/config');

module.exports = function (app, passport) {
    app.post('/signup',
        passport.authenticate('local-signup', {
            successRedirect: '/signup/success',
            failureRedirect: '/failure',
            failureFlash: true
        })
    );
    app.get('/failure', function (req, res) {
        // If this function gets called, authentication was successful.
        // `req.user` contains the authenticated user.
        res.json(Response.error(req.flash('failure')[0]));
    });
   

    app.get('/login/failure', function (req, res) {
        // If this function gets called, authentication was successful.
        // `req.user` contains the authenticated user.
        res.json(Response.error(req.flash('failure')[0]));
    });
    // route for facebook authentication and login
    app.get('/auth/facebook', passport.authenticate('facebook', { scope: 'email' }));

    // handle the callback after facebook has authenticated the user
    app.get('/auth/facebook/callback',
        passport.authenticate('facebook'
            , {
                successRedirect: '/success',
                failureRedirect: '/login/failure'
            }
        ));

    // route for logging out
    app.get('/logout', function (req, res) {
        req.logout();
        res.redirect('/login/failure');
    });
    app.get('/success', function (req, res) {
        var id = req.user._id;
        setTimeout(
            () => {
                
                res.redirect(config.callback + "?id=" + id);
            }, 2000
        );
    });
    app.get('/success/login', function (req, res) {
       
        var id = req.user._id;
        setTimeout(
            () => {
               
                res.redirect(config.callback + "?id=" + id);
            }, 2000
        );
    });
    app.get('/signup/success', function (req, res) {

        res.json(Response.get(req.flash("success")));
    });

    app.get('/auth/twitter', passport.authenticate('twitter'));

    // handle the callback after twitter has authenticated the user
    app.get('/auth/twitter/callback',
        passport.authenticate('twitter', {
            successRedirect: '/success',
            failureRedirect: '/login/failure'
        }));

    app.get('/auth/google', passport.authenticate('google', { scope: ['profile', 'email'] }));

    // the callback after google has authenticated the user
    app.get('/auth/google/callback',
        passport.authenticate('google', {
            successRedirect: '/success',
            failureRedirect: '/login/failure'
        }));



};
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/login/failure');
}

