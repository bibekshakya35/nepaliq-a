var express = require('express');
var router = express.Router();

var cors = require('cors');
router.use(cors());
router.options('*', cors());

var mongoose = require('mongoose');
var Response = require('../model/response');
//bring mongo js
var mongojs = require('mongojs');
//connect with database
var config = require('../model/config');

var db = mongojs(config.dbUrl, ['loggers']);


router.post('/log', (req, res) => {
    var logger = req.body;
    logger.createdOn = new Date();
    db.loggers.save(logger, function (err, logger) {
        if (err) {
            res.send(err);
        }
        res.json(Response.created("logger is saved", logger));
    });

});
router.post("/logs", (req, res) => {
    var loggers = req.body;
    db.loggers.insert(loggers,
        function (err, loggers) {
            if (err) {
                res.send(err);
            }
        });
});
router.get('/log/:userid/notification', (req, res) => {
    let id =
        req.params.userid;

    db.loggers.find(
        {
            "userid": id,
            "status": true,
            "type": "NOTIFICATION"
        }
        , function (err, loggers) {
            if (err) {
            
            }
          
            res.json(Response.get(loggers));
        });
}
);

router.put('/log/:id/read', (req, res) => {
    let id =
        req.params.id;
    let status = req.body.status;
    db.loggers.update(
        {
            _id: mongojs.ObjectId(id)
        },
        { $set: { "status": status } }
        , function (err, logger) {
            if (err) {
                
            }
         
            res.json(Response.get(logger));
        });
});
router.get("/logs/post/:userid/:postid", (req, res, next) => {
    let userid = req.params.userid;
    let postid = req.params.postid;

    db.loggers.findOne({
        "userid": userid,
        "postid": postid,
        "type": "VOTE"
    },
        (err, logger) => {
           
            if (logger) {
                return res.json(Response.get({ "status": false }));
            }
            else {

                return res.json(Response.get({ "status": true }));
            }
        }

    );
});
router.get("/logs/answer/:userid/:answerid", (req, res, next) => {
    let userid = req.params.userid;
    let answerid = req.params.answerid;
    db.loggers.findOne({
        "userid": userid,
        "answerid": answerid,
        "type": "VOTE"
    },
        (err, logger) => {
            if (logger) {
                return res.json(Response.get({ "status": false }));
            }
            else {
                return res.json(Response.get({ "status": true }));
            }

        }
    )
});
module.exports = router;