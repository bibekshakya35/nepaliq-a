var api = "AIzaSyDh2I0PbJ7o2IYIR_deS8xPbzZj-rCj94Y";
import { getPlace } from '../service/placeservice';

var validator = require('validator');

var GooglePlaces = require('google-places');

var googlePlaces = new GooglePlaces(api);

var googleMapsClient = require('@google/maps').createClient({
  key: api
});

var multer = require('multer');
var mime = require('mime-types');
var crypto = require('crypto');
var storage = multer.diskStorage({
  destination: 'views/pages/images/story',
  filename: function (req, file, cb) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
      console.log(err);
      cb(null, raw.toString('hex') + "." + mime.extension(file.mimetype));
    });
  }
});

var upload = multer({ storage: storage })
var express = require('express');
var router = express.Router();

var cors = require('cors');
router.use(cors());
router.options('*', cors());

var Response = require('../model/response');
var ps = require('../model/placestory');
//bring mongo js
var mongojs = require('mongojs');

//connect with database
var config = require('../model/config');

var db = mongojs(config.dbUrlNearBy, ['placestory']);

router.post('/nearby/image/upload', upload.any(), (req, res, next) => {
  console.log("upload");
  let files = [];
  files = req.files;

  res.send(files);
});

router.get('/nearby/search/:place', (req, res, next) => {
  let placeToGo = req.params.place;

  let placeId;
  let googleMapDescription;
  let googlePlaceDescription;
  googleMapsClient.geocode({
    address: placeToGo + " Nepal"
  }, function (err, response) {
    if (!err) {
      if (response.json.status === "ZERO_RESULTS") {
        console.log("PLEASE PLACE VALID ADDRESS");
        return;
      }
      googleMapDescription = response.json.results[0];
      placeId = response.json.results[0].place_id;
      googlePlaces.details({ place_id: placeId }, function (err, response) {
        googlePlaceDescription = response.result;
        return res.json(Response.get(getPlace(googleMapDescription, googlePlaceDescription, api, placeToGo)));
      });
    }
  });

});
router.get('/nearby/places/story', (req, res, next) => {
  db.placestory.find().sort({ "createdOn": -1 }, function (err, posts) {
    res.json(Response.get(posts));
  });
})
router.post('/nearby/addplacestory', (req, res, next) => {
  let placeStory = req.body;
  placeStory.createdDate = new Date();
  if (!ps.checkIfObjectIsWellForPlace(placeStory)) {
    return res.json(Response.error("Request have insufficient property"));
  }
  if (!validator.isLength(placeStory.place, { min: 5, max: 15 })) {
    return res.json(Response.error("Place name should be length between 5 to 15"));
  }
  if (!validator.isLength(placeStory.story, { min: 5, max: 120 })) {
    return res.json(Response.error("Story should be length between 5 to 120"));
  }
  if (!validator.isLength(placeStory.picture, { min: 10 })) {
    return res.json(Response.error("Picture is not given"));
  }
  if (!validator.isLength(placeStory.userid, { min: 5 }) && !validator.isLength(placeStory.name, { min: 3 })) {
    return res.json(Response.error("user is not verified"))
  }

  db.placestory.find({ "picture": placeStory.picture }, (err, nb) => {
    if (nb.length === 0) {

      db.placestory.save(placeStory, (err, nb) => {
        if (err) {
          return res.json(Response.error("Unable to fulfill your request"));
        }
        console.log("STORE PLACE STORY");
        return res.json(Response.success("Your story has been store", {}));
      });
    }
    else {
      return res.json(Response.error("You are posting same thing or duplicate content"));
    }
  });

});

module.exports = router;

