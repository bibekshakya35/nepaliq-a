var express = require('express');
var router = express.Router();

var cors = require('cors');
router.use(cors());
router.options('*', cors());

var User = require('../model/user');
var Response = require('../model/response');
//bring mongo js
var mongojs = require('mongojs');
//connect with database
var config = require('../model/config');

var db = mongojs(config.dbUrl, ['users']);



router.get('/user/:id', (req, res, next) => {
    //call back function
    if (req.params.id == undefined) {
        return;
    }
    db.users.findOne({ _id: mongojs.ObjectId(req.params.id) }, (err, user) => {

        if (err) {
            res.json(Response.error("Unable to retrive"));
        }
        user.password = "";
        res.json(Response.get(user));
    });
});

router.put('/user/:id/reputation', (req, res, next) => {
    let body = req.body.data;
    if (typeof body === 'number') {
        db.users.update(
            { "userid": req.params.id },
            { $inc: { reputation: body } }
        );
    }
});
router.put("/user/:id/favourites", (req, res, next) => {
    let favourites = req.body;
    db.users.update(
        {
            _id: mongojs.ObjectId(req.params.id)
        },
        {
            $push: {
                "favourites": favourites
            }
        },
        (error, post) => {
            if (error) {
                return res.json(Response.error("Problem"));
            }
            else {
                return res.json(Response.success("Favourites categories insert"));
            }
        }

    )
});
router.get("/user/favourites/:id", (req, res, next) => {
    if (req.params.id == undefined) {
        return;
    }
    db.users.findOne({ _id: mongojs.ObjectId(req.params.id) }, (err, user) => {

        if (err) {
            return res.json(Response.error("Unable to retrive"));
        }
        if (user.favourites) {
            return res.json(Response.get(user.favourites[0]));
        }
        else {
            return res.json(Response.error("Unable to retrive"));
        }
    });
})
router.post('/login', (req, res, next) => {
    var loginData = req.body;

    // find a user whose email is the same as the forms email
    // we are checking to see if the user trying to login already exists
    User.findOne({ 'email': loginData.email }, function (err, user) {
        // if there are any errors, return the error before anything else
        if (err)
            return res.json(Response.error(err));

        // if no user is found, return the message
        if (!user)
            return res.json(Response.error("User not found with given credential"));

        // if the user is found but the password is wrong
        if (!user.validPassword(loginData.password))
            return res.json(Response.error("OOPS! Password is incorrect"));
        user.password = "";
        console.log("User is logged in", user.name);
        return res.json(Response.success("Login success", user));

    });
});

module.exports = router;