var express = require('express');
var router = express.Router();

var cors = require('cors');
router.use(cors());
router.options('*', cors());


var validator = require('validator');
var Response = require('../model/response');
//bring mongo js
var mongojs = require('mongojs');
//connect with database
var config = require('../model/config');

var db = mongojs(config.dbUrl, ['bloods']);

var dbDonar = mongojs(config.dbBloodDonar);

var donarservice = require("../service/donarservice");

router.post("/bloods/donars", (req, res, next) => {
    let donar = req.body;
    console.log("blood donar found");
    donar.createdDate = new Date();
    let response = donarservice.checkIfDonarObjectWell(donar);
    donar.location = donar.location.toLocaleLowerCase();
    donar.status = true;
    if (response === null) {
        dbDonar.donar.find({ "email": donar.email }, (err, donars) => {
            if (donars.length > 0) {
                return res.json(Response.error("please use another email address this one is taken"));
            }
            else {
                dbDonar.donar.insert(donar, (err, donar) => {
                    console.log("blood donar saved");
                    return res.json(Response.success("Insert", {}));
                });
            }
        })
    }
    else {
        return res.json(response);
    }
});

//get All the blood 
router.get('/bloods', (req, res, next) => {
    //call back function let data = req.body;

    db.bloods.find(function (err, bloods) {
        if (err) {
            res.send(err);
        }
        res.json(Response.get(bloods));
    });
});
router.get('/blood/:id', (req, res, next) => {
    //call back function

    db.bloods.findOne({ _id: mongojs.ObjectId(req.params.id) }, (err, blood) => {
        if (err) {
            res.send(err);
        }
        res.json(Response.get(blood));
    });
});
//save bloods
router.post("/blood", function (req, res, next) {
    let blood = req.body;
    if (blood.title == "") {
        response.error("Tittle not found");
    }
    else {
        db.bloods.save(blood, function (err, blood) {
            if (err) {
                res.send(err);
            }
            res.json(Response.created("blood is saved", blood));
        });
    }
});
router.put("/blood/:id", function (req, res, next) {
    var blood = req.body;
    if (!blood) {
        res.json(
            Response.error("No blood found")
        );
    }
    else {
        db.bloods.update({ _id: mongojs.ObjectId(req.params.id) }, blood, {}, function (err, blood) {
            if (err) {
                res.send(err);
            }
            res.json(Response.updated("blood is Updated"));
        });
    }
});

router.delete("/blood/:id", function (req, res, next) {
    db.bloods.remove({ _id: mongojs.ObjectId(req.params.id) }, function (err, blood) {
        console.log("server side" + JSON.stringify(blood));
        if (err) {
            res.send(err);
        }
        res.json(Response.success("blood is delete", blood));
    });
});
router.put("/blood/:id/addanswer", function (req, res, next) {
    var answer = req.body;
    db.bloods.update(
        {
            _id: mongojs.ObjectId(req.params.id)
        },
        {
            $push: {
                "answers": answer
            }
        },
        (error, blood) => {
            if (error) {
                res.json(Response.error("Answer unable to blood"));
            }
            else {
                res.json(Response.success("your answer has beem submitted"));
            }
        }

    )
});
router.post("/blood/bulk", function (req, res, next) {
    let bloods = req.body;
    bloods.forEach(function (obj) {
        obj.status = true;
    });
    db.bloods.insert(bloods, function (err, bloods) {
        if (err) {
            res.send(err);
        }
        res.json(Response.created("blood is saved", bloods));
    });

});
router.get("/blooddonar/search/:bloodGroup", function (req, res, next) {
    let bloodGroup = req.params.bloodGroup;
    let bloodsData = [];
    dbDonar.donar.find(
        {
            "bloodGroup": bloodGroup
        }, (err, bloods) => {
            bloods.forEach((e) => {
                let donar = {
                    "name": e.name,
                    "bloodgroup": bloodGroup,
                    "emailid": e.emailid,
                    "avatarUrl": e.avatarUrl,
                    "location": e.location,
                    "city": e.city,
                    "mobile": e.mobile
                }
                bloodsData.push(donar);
            });
            db.bloods.find({
                "bloodgroup": bloodGroup
            }, (err, bloods) => {
                bloods.forEach((e) => {
                    let donar = {
                        "name": e.name,
                        "bloodgroup": bloodGroup,
                        "emailid": e.emailid,
                        "avatarUrl": "/assets/img/imagenotfound.png",
                        "location": e.location,
                        "city": e.city,
                        "mobile": e.mobile
                    }
                    bloodsData.push(donar);
                });
                if (bloodsData.length == 0) {
                    dbDonar.donar.find(
                        {
                            "bloodGroup": bloodGroup
                        }, (err, bloods) => {
                            bloods.forEach((e) => {
                                let donar = {
                                    "name": e.name,
                                    "bloodgroup": bloodGroup,
                                    "emailid": e.emailid,
                                    "avatarUrl": e.avatarUrl,
                                    "location": e.location,
                                    "city": e.city,
                                    "mobile": e.mobile
                                }
                                bloodsData.push(donar)
                            });
                            db.bloods.find({
                                "bloodgroup": bloodGroup
                            }, (err, bloods) => {
                                bloods.forEach((e) => {
                                    let donar = {
                                        "name": e.name,
                                        "bloodgroup": bloodGroup,
                                        "emailid": e.emailid,
                                        "avatarUrl": "/assets/img/imagenotfound.png",
                                        "location": e.location,
                                        "city": e.city,
                                        "mobile": e.mobile
                                    }
                                    bloodsData.push(donar);
                                });
                                return res.json(Response.get(bloodsData));
                            });
                        });
                }
                else {
                    return res.json(Response.get(bloodsData));
                }
            }
            );
        }
    );
})
router.get("/blooddonar/search/:bloodGroup/:location", function (req, res, next) {
    let location = req.params.location;
    let bloodGroup = req.params.bloodGroup;
    let bloodsData = [];
    dbDonar.donar.find(
        {
            "location": { '$regex': location, '$options': 'i' },
            "bloodGroup": bloodGroup
        }, (err, bloods) => {
            bloods.forEach((e) => {
                let donar = {
                    "name": e.name,
                    "bloodgroup": bloodGroup,
                    "emailid": e.emailid,
                    "avatarUrl": e.avatarUrl,
                    "location": e.location,
                    "city": e.city,
                    "mobile": e.mobile
                }
                bloodsData.push(donar);
            });
            db.bloods.find({
                "location": { '$regex': location, '$options': 'i' },
                "bloodgroup": bloodGroup
            }, (err, bloods) => {
                bloods.forEach((e) => {
                    let donar = {
                        "name": e.name,
                        "bloodgroup": bloodGroup,
                        "emailid": e.emailid,
                        "avatarUrl": "/assets/img/imagenotfound.png",
                        "location": e.location,
                        "city": e.city,
                        "mobile": e.mobile
                    }
                    bloodsData.push(donar);
                });
                if (bloodsData.length == 0) {
                    dbDonar.donar.find(
                        {
                            "bloodGroup": bloodGroup
                        }, (err, bloods) => {
                            bloods.forEach((e) => {
                                let donar = {
                                    "name": e.name,
                                    "bloodgroup": bloodGroup,
                                    "emailid": e.emailid,
                                    "avatarUrl": e.avatarUrl,
                                    "location": e.location,
                                    "city": e.city,
                                    "mobile": e.mobile
                                }
                                bloodsData.push(donar)
                            });
                            db.bloods.find({
                                "bloodgroup": bloodGroup
                            }, (err, bloods) => {
                                bloods.forEach((e) => {
                                    let donar = {
                                        "name": e.name,
                                        "bloodgroup": bloodGroup,
                                        "emailid": e.emailid,
                                        "avatarUrl": "/assets/img/imagenotfound.png",
                                        "location": e.location,
                                        "city": e.city,
                                        "mobile": e.mobile
                                    }
                                    bloodsData.push(donar);
                                });
                                return res.json(Response.get(bloodsData));
                            });
                        });
                }
                else {
                    return res.json(Response.get(bloodsData));
                }
            }
            );
        }
    );
})
module.exports = router;