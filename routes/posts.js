var express = require('express');
var router = express.Router();

var cors = require('cors');
router.use(cors());
router.options('*', cors());

var Response = require('../model/response');
//bring mongo js
var mongojs = require('mongojs');

//connect with database
var config = require('../model/config');

var db = mongojs(config.dbUrl, ['posts']);


//get All the post 
router.get('/posts', (req, res, next) => {
    //call back function
    console.log("----------------------------------------");
    console.log('User-Agent: ' + req.headers['user-agent']);
    console.log("----------------------------------------");
    db.posts.find().sort({ "date": -1 }, function (err, posts) {
        res.json(Response.get(posts));
    });
});

router.get('/post/:id', (req, res, next) => {
    //call back function

    db.posts.findOne({ _id: mongojs.ObjectId(req.params.id) }, (err, post) => {
        if (err) {
            res.json(Response.error(err));
        }
        res.json(Response.get(post));
    });
});
//save posts
router.post("/post", function (req, res, next) {
    let post = req.body;
    let sortedAndLowerCase = [];
    post.category.split(",").forEach(function (element) {
        sortedAndLowerCase.push(element.trim().replace(/\s/g, '').toLocaleLowerCase());
    });
    post.categories = sortedAndLowerCase;
    db.posts.save(post, function (err, post) {
        if (err) {
            res.send(err);
        }
        console.log("POST IS ADDED");
        res.json(Response.created("Post is saved", post));
    });

});

router.put("/post/:id", function (req, res, next) {
    var post = req.body;
    if (!post) {
        res.json(
            Response.error("No Post found")
        );
    }
    else {
        db.posts.update({ _id: mongojs.ObjectId(req.params.id) }, post, {}, function (err, post) {
            if (err) {
                res.send(err);
            }
            res.json(Response.updated("Post is Updated"));
        });

    }
});

router.delete("/post/:id", function (req, res, next) {
    db.posts.remove({ _id: mongojs.ObjectId(req.params.id) }, function (err, post) {

        if (err) {
            res.send(err);
        }
        res.json(Response.success("Post is delete", post));
    });
});
//add answer
router.put("/post/:id/addanswer", function (req, res, next) {
    var answer = req.body;
    answer.id = makeId();

    db.posts.update(
        {
            _id: mongojs.ObjectId(req.params.id)
        },
        {
            $push: {
                "answers": answer
            }
        },
        (error, post) => {
            if (error) {
                res.json(Response.error("Answer unable to post"));
            }
            else {
                console.log("Answer IS ADDED");
                res.json(Response.success("your answer has beem submitted"));
            }
        }

    )
});
router.get("/posts/categories/:type1", (req, res, next) => {

    var type =
        req.params.type1.split(",");
    db.posts.find({
        "categories": { $all: type }
    }).sort({ "createdOn": -1 }, function (err, posts) {
        if (err) {

            res.send(err);
        }

        res.json(Response.get(posts));
    });
});
router.get("/post/:title/:date", (req, res, next) => {
    let title = req.params.title;
    let date = req.params.date;
    db.posts.find({
        "title": title,
        "date": date
    },
        (err, post) => {
            if (err) {
                return res.json(Response.error("Unable to find post"));
            }

            return res.json(Response.get(post));
        }
    )
});

router.get("/allpost/:userid", function (req, res, next) {
    let userid = req.params.userid;
    if (userid === null || userid === undefined || userid.length < 0) {
        return res.json(Response.error("Invalid Request"));
    }
    else {
        db.posts.find({ "userid": userid }).sort({ "date": -1 }, (err, posts) => {
            if (posts) {
                return res.json(Response.get(posts));
            }
        });
    }
});
router.get("/allanswer/:userid", function (req, res, next) {
    let userid = req.params.userid;
    if (userid === null || userid === undefined || userid.length < 0) {
        return res.json(Response.error("Invalid Request"));
    }
    else {
        db.posts.find({ "answers.userid": userid }).sort({ "date": -1 }, (err, posts) => {
            if (posts) {
                return res.json(Response.get(posts));
            }
        });
    }
});
router.get("/allcategory/:userid",
    function (req, res, next) {
        let userid = req.params.userid;
        if (userid === null || userid === undefined || userid.length < 0) {
            return res.json(Response.error("Invalid Request"));
        }
        else {
            db.posts.find({ $or: [{ "userid": userid }, { "answers.userid": userid }] }).sort({ "date": -1 }, (err, posts) => {
                let categories = new Set();
                if (posts) {
                    posts.forEach(function (element) {
                        if (element.categories) {
                            element.categories.forEach(function (element) {
                                categories.add(element);
                            });
                        }
                        else{
                            categories.add("general");
                        }
                    });
                    return res.json(Response.get(categories));
                }
            });
        }
    });
// function isLoggedIn(req, res, next) {

//     // if user is authenticated in the session, carry on
//     if (req.isAuthenticated())
//         return next();

//     // if they aren't redirect them to the home page
//     res.redirect('/login/failure');
// }
module.exports = router;

function makeId() {
    let text = "";
    let possible = "DJKSLAHHHQEKHKJHAKJHKJSHADKHSDAKHDKSAHKDHASKHBQWMNBEMNBQXOIAS";
    for (let i = 0; i < 5; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}