var express = require('express');
var router = express.Router();

var cors = require('cors');
router.use(cors());
router.options('*', cors());
var Response = require('../model/response');
//bring mongo js
var mongojs = require('mongojs');
//connect with database
var config = require('../model/config');

var db = mongojs(config.dbUrlChat);

router.get('/gc/history/:room', (req, res, next) => {
    //call back function
    var room = req.params.room;
    db.group_chat.find({ "room": room }, (err, chats) => {
        if (err) {
            res.json(Response.error(err));
        }
        console.log("rooms",chats)
        res.json(Response.get(chats));
    });
});
router.post("/chatrooms", (req, res, next) => {
    var chatRoom = req.body;
    chatRoom.createdDate = new Date();
    db.chatroom.insert(chatRoom, (err, chatRoom) => {
        return res.json(Response.created("Success", {}));
    });
});
router.get("/chatrooms", (req, res, next) => {
    db.chatroom.find({}, (err, chatrooms) => {
        return res.json(Response.get(chatrooms));
    })
});
router.get("/chatrooms/:id", (req, res, next) => {
    db.chatroom.findOne({ _id: mongojs.ObjectId(req.params.id) }, (err, chatroom) => {
        return res.json(Response.get(chatroom));
    });
})
module.exports = router;