export function getPlace(googleMap, googlePlace, key, placeToGo) {
    let addressComponents = googleMap.address_components;

    let galleries = [];
    //console.log(JSON.stringify(googleMap.address_components));
    let sortedGoogleMap = sortData(googleMap);
    if (sortedGoogleMap.place == null || sortedGoogleMap.place == undefined || sortedGoogleMap.place == "") {
        console.log("HERE");
        sortedGoogleMap.place = placeToGo.capitalizeFirstLetter();
    }
    sortedGoogleMap.gmap = googleMap.geometry;
    if (googlePlace) {
        googlePlace.photos.forEach(function (element) {
            let preFix = "https://maps.googleapis.com/maps/api/place/photo?maxwidth=800";
            preFix += "&photoreference=" + element.photo_reference;
            preFix += " &key=" + key;
            galleries.push(preFix);
        }, this);
    }
    sortedGoogleMap.galleries = galleries;
    //console.log(JSON.stringify(sortedGoogleMap));
    return sortedGoogleMap;
};

function sortData(googleMap) {
    let place, city, zone, region, highway;

    let addressCmp = googleMap.address_components;
    addressCmp.filter((e, indexq) => {
        let locationType = e.types;
        locationType.filter((e, index) => {
            switch (e) {
                case "natural_feature": {
                    place = addressCmp[indexq].long_name;
                    break;
                }
                case "sublocality": {
                    place = addressCmp[indexq].long_name;
                    break;
                }
                case "locality": {
                    city = addressCmp[indexq].long_name;
                    break;
                }
                case "administrative_area_level_2": {
                    zone = addressCmp[indexq].long_name;
                    break;
                }
                case "administrative_area_level_1": {
                    region = addressCmp[indexq].long_name;
                    break;
                }
                case "route": {
                    highway = addressCmp[indexq].long_name;
                    break;
                }
                case "administrative_area_level_3": {
                    if (city != null || "") {
                        city += " " + addressCmp[indexq].long_name;
                    }
                    city = addressCmp[indexq].long_name;
                }
            }

        });

    });
    return {
        "place": place,
        "city": city,
        "zone": zone,
        "region": region,
        "highway": highway
    };
}
String.prototype.capitalizeFirstLetter = function () {
    return this.charAt(0).toUpperCase() + this.slice(1).toLowerCase();
}