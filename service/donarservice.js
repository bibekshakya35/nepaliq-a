var validator = require('validator');
var Response = require('../model/response');


var mongojs = require('mongojs');
var config = require('../model/config');
var dbDonar = mongojs(config.dbBloodDonar);

var donarservice = require("../service/placeservice");
var donar = {
    "name": undefined,
    "location": undefined,
    "city": undefined,
    "dob": undefined,
    "emailid": undefined,
    "mobile": undefined,
    "bloodgroup": undefined,
    "contactTime": undefined,
    "disease": undefined,
    "lastBloodDuration": undefined,
    "createdTime": undefined,
    "status":undefined
}
var bloodGroups = ["O−", "O+", "A−", "A+", "B−", "B+", "AB−", "AB+"];

module.exports.checkIfDonarObjectWell = function (donar) {
    for (let i in donar) {
        if (!donar.hasOwnProperty(i)) {
            return Response.error("Request is incomplete");
        }
    }
    if (!validator.isLength(donar.name, { min: 5 })) {
        return Response.error("Donar name should not be less than 5");
    }
    if (!validator.isLength(donar.location, { min: 2 })) {
        return Response.error("Give us valid current location you stayed in");
    }
    if (!validator.isLength(donar.city, { min: 4 })) {
        return Response.error("Please select city");
    }
    if (!validator.isLength(donar.dob, { min: 4 })) {
        return Response.error("choose correct date of birth");
    }
    // if (checkDob(donar.dob <= 16)) {
    //     return Response.error("You have to 16 year old to give blood");
    // }
    if (!validator.isEmail(donar.email)) {
        return Response.error("give us a valid email address");
    }
    if (!validator.isNumeric(donar.mobile)) {
        return Response.error("give us a valid mobile number");
    }
    return null;
}

function checkDob(dob) {
    var today = new Date();
    var birthDate = new Date(dob);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    console.log(age);
    return age;
}